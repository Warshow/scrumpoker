<?php
namespace controllers;
use controllers\crud\datas\UserCrudControllerDatas;
use Ubiquity\controllers\crud\CRUDDatas;
use controllers\crud\viewers\UserCrudControllerViewer;
use Ubiquity\controllers\admin\viewers\ModelViewer;
use controllers\crud\events\UserCrudControllerEvents;
use Ubiquity\controllers\crud\CRUDEvents;
use controllers\crud\files\UserCrudControllerFiles;
use Ubiquity\controllers\crud\CRUDFiles;
use Ubiquity\utils\http\URequest;

 /**
 * CRUD Controller UserCrudController
 * @route("/UserCrud","inherited"=>true,"automated"=>true)
 **/
class UserCrudController extends \Ubiquity\controllers\crud\CRUDController{

	public function __construct(){
		parent::__construct();
		$this->model="models\\User";
	}

	public function _getBaseRoute() {
		return '/UserCrud';
	}
	
	public function initialize(){
	    if(!URequest::isAjax()){
	        $this->loadView("main/vHeader.html");
	    }
	}
	
	protected function getAdminData(): CRUDDatas{
		return new UserCrudControllerDatas($this);
	}

	protected function getModelViewer(): ModelViewer{
		return new UserCrudControllerViewer($this);
	}

	protected function getEvents(): CRUDEvents{
		return new UserCrudControllerEvents($this);
	}

	protected function getFiles(): CRUDFiles{
		return new UserCrudControllerFiles();
	}


}
