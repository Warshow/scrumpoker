<?php
namespace controllers;
use controllers\crud\datas\ProjectCrudControllerDatas;
use Ubiquity\controllers\crud\CRUDDatas;
use controllers\crud\viewers\ProjectCrudControllerViewer;
use Ubiquity\controllers\admin\viewers\ModelViewer;
use controllers\crud\events\ProjectCrudControllerEvents;
use Ubiquity\controllers\crud\CRUDEvents;
use controllers\crud\files\ProjectCrudControllerFiles;
use Ubiquity\controllers\crud\CRUDFiles;

 /**
 * CRUD Controller ProjectCrudController
 * @route("/ProjectCrudController","inherited"=>true,"automated"=>true)
 **/
class ProjectCrudController extends \Ubiquity\controllers\crud\CRUDController{

	public function __construct(){
		parent::__construct();
		$this->model="models\\Project";
	}

	public function _getBaseRoute() {
		return '/ProjectCrudController';
	}
	
	protected function getAdminData(): CRUDDatas{
		return new ProjectCrudControllerDatas($this);
	}

	protected function getModelViewer(): ModelViewer{
		return new ProjectCrudControllerViewer($this);
	}

	protected function getEvents(): CRUDEvents{
		return new ProjectCrudControllerEvents($this);
	}

	protected function getFiles(): CRUDFiles{
		return new ProjectCrudControllerFiles();
	}


}
