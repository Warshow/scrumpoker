<?php
namespace controllers;

use Ubiquity\orm\DAO;
use models\Serie;
use models\Project;
use Ubiquity\controllers\auth\AuthController;
use Ubiquity\controllers\auth\WithAuthTrait;
use models\User;
use Ubiquity\utils\http\URequest;
use models\Story;
 /**
 * Controller BoardController
 * @property \Ajax\php\ubiquity\JsUtils $jquery
 **/
class BoardController extends ControllerBase{
    use WithAuthTrait;
    protected function getAuthController(): AuthController {
        return new BaseAuthController();
    }
    public function index(){
        ;
    }
    /**
     *@get("Board/{idProject}","requirements"=>["idProject"=>"\d+"])
     **/
    public function Board($idProject){
        $project=DAO::getOne(Project::class, $idProject,true);
        $serie=DAO::getOne(Serie::class,"2");
        $serie=explode(",", $serie->getPoints());
        
        $this->jquery->ajaxInterval("post", "loadUser", 500,null,"#user_online",["online"=>true, "hasLoader"=>false]);
        
        $this->jquery->counterOn("#startButton", "click", ".counter",0,5,"_counter",false);
        $this->jquery->click("#startButton",$this->jquery->addClass("#startButton","disabled"));
        $this->jquery->execOn("counter-end", ".counter", $this->jquery->ajax("post","actualStory/$idProject","#story_actuel",[true]));
        $this->jquery->execOn("click", "#stopButton", $this->jquery->clearInterval("_counter",false));
        $this->jquery->click("#stopButton",$this->jquery->removeClass("#startButton","disabled"));
        
        //$this->jquery->ajaxOn("counter-end", ".counter", "actualStory","#story_actuel",["statut"=>true]);
        
        $this->jquery->renderView("BoardController/index.html",["serie"=>$serie,"project"=>$project]);
	}
	
	
	
	/**
	 *@route("loadUser","methods"=>["post"])
	 **/
	public function loadUser() {
	    $online = URequest::post("online",true);
	    if (URequest::isAjax()) {
	        $user_online=DAO::getAll(User::class,"online=?", false,[$online]);
	        foreach($user_online as $user){
	            echo "<div class='item'> <div class='content'><i class='user icon'></i>".$user->getName()."</div></div>";
	        }
	    }
	}
	/**
	 *@post("actualStory/{idProject}","requirements"=>["idProject"=>"\d+"])
	 **/
	public function actualStory($idProject) {
	    $statut = 1;//URequest::post("statut",true);
	    //$idProject = URequest::post("idProject");
	    //var_dump($idProject);
	    if (URequest::isAjax()) {
	        $story_act=DAO::getAll(Story::class,'idProject=?',false,[$idProject]);
	        $story_vote=DAO::getOne(Story::class,'statut=? and idProject=?',false,[$statut,$idProject]);
	        foreach ($story_act as $sto) {
	            if ($sto = $story_vote) {
	                echo "<div class='item'>---".$sto->getName()."---</div>";
	            }else {
	            echo "<div class='item'>".$sto->getName()."</div>";
	            }
	        }
	    }
	}
}
