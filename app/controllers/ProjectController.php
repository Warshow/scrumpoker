<?php
namespace controllers;
 use Ubiquity\orm\DAO;
use Ubiquity\utils\http\URequest;
use models\Project;
use Ajax\php\ubiquity\JsUtils;

 /**
 * Controller ProjectController
 * @property \Ajax\php\ubiquity\JsUtils $jquery
 **/
class ProjectController extends ControllerBase{
    
    /**
     *@route("test","methods"=>["get"])
     **/
	public function index(){
	    $projects=DAO::getAll(Project::class);
		$this->jquery->renderView("ProjectController/index.html",["projects"=>$projects]);
	}

	/**
	 *@route("ProjectList","methods"=>["get"])
	**/
	public function ProjectList(){
	    $projects=DAO::getAll(Project::class);
	    $this->jquery->renderView('ProjectController/ProjectList.html',["projects"=>$projects]);

	}
	
	/**
	 *@get("project/stories/{idProject}","requirements"=>["idProject"=>"\d+"])
	 **/
	public function stories($idProject,$asString=false){
	    if(URequest::isAjax() || $asString===true){
	        $project=DAO::getOne(Project::class,$idProject,["storys"]);
	       $this->jquery->getOn("mouseenter", "._stories", "project/story/","#divEstimations",["attr"=>"data-ajax"]);
	       
	        $this->jquery->renderView('ProjectController/stories.html',compact("project"),$asString);
	    }else{
	        $this->all($idProject);
	    }
	}
	
	/**
	 *@get("project/form/{idProject}","requirements"=>["idProject"=>"\d+"])
	 **/
	public function form($idProject) {
	    $project=DAO::getOne(Project::class, $idProject);
	    $this->jquery->renderView('ProjectController/form.html',["project"=>$project]);
	}
	    

}
