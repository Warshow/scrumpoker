<?php
namespace controllers\crud\files;

use Ubiquity\controllers\crud\CRUDFiles;
 /**
 * Class ProjectCrudControllerFiles
 **/
class ProjectCrudControllerFiles extends CRUDFiles{
	public function getViewIndex(){
		return "ProjectCrudController/index.html";
	}

	public function getViewForm(){
		return "ProjectCrudController/form.html";
	}

	public function getViewDisplay(){
		return "ProjectCrudController/display.html";
	}


}
