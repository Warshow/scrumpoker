<?php
namespace controllers\crud\files;

use Ubiquity\controllers\crud\CRUDFiles;
 /**
 * Class UserCrudControllerFiles
 **/
class UserCrudControllerFiles extends CRUDFiles{
	public function getViewIndex(){
		return "UserCrudController/index.html";
	}

	public function getViewForm(){
		return "UserCrudController/form.html";
	}

	public function getViewDisplay(){
		return "UserCrudController/display.html";
	}


}
