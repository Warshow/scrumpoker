<?php
namespace controllers\crud\viewers;

use Ubiquity\controllers\admin\viewers\ModelViewer;
 /**
 * Class UserCrudControllerViewer
 **/
class UserCrudControllerViewer extends ModelViewer{
	/**
     * {@inheritDoc}
     * @see \Ubiquity\controllers\admin\viewers\ModelViewer::getModelDataTable()
     */
    public function getModelDataTable($instances, $model, $totalCount, $page = 1)
    {
        // TODO Auto-generated method stub
        $dataTbale =parent::getModelDataTable($instances, $model, $totalCount, $page = 1);
        $dataTbale->setInverted(true);
        return ;
    }

    /**
     * {@inheritDoc}
     * @see \Ubiquity\controllers\admin\viewers\ModelViewer::getDataTableRowButtons()
     */
    protected function getDataTableRowButtons()
    {
        // TODO Auto-generated method stub
        return ["edit","display","delete"];
    }

    //use override/implement Methods
	
    public function getFormTitle($form,$instance) {
        
        return ["icon"=>"blind","message"=>"Ajouter","subMessage"=>"Un ivoirien"];
    }
    
    
    /**
     * {@inheritDoc}
     * @see \Ubiquity\controllers\admin\viewers\ModelViewer::onDataTableRowButton()
     */
    public function onDataTableRowButton(\Ajax\semantic\html\elements\HtmlButton $bt)
    {
        // TODO Auto-generated method stub
        if ($bt->propertyContains("class", "_edit")) {
            $bt->setColor("green");
        }
        if ($bt->propertyContains("class", "_display")) {
            $bt->setInverted(true);
        }
        $bt->setSize("mini");
    }

    public function getForm($identifier, $instance)
    {
        $frm= parent::getForm($identifier, $instance);
        $frm->fieldAsHidden("id");
        return $frm;
    }
    
    
}
