<?php
namespace controllers;
use Ubiquity\utils\http\USession;
use Ubiquity\utils\http\URequest;
use Ubiquity\utils\flash\FlashMessage;
use Ubiquity\controllers\Startup;

use Ubiquity\orm\DAO;
use models\User;

 /**
 * Auth Controller BaseAuthController
 **/
class BaseAuthController extends \Ubiquity\controllers\auth\AuthController{

	protected function onConnect($connected) {
		$urlParts=$this->getOriginalURL();
		USession::set($this->_getUserSessionKey(), $connected);
		if(isset($urlParts)){
		    Startup::forward(implode("/",$urlParts));
		}else{
		    Startup::forward("Board");
			//TODO
			//Forwarding to the default controller/action
		}
	}

	protected function _connect() {
		if(URequest::isPost()){
			$email=URequest::post($this->_getLoginInputName());
			$password=URequest::post($this->_getPasswordInputName());
			$user = DAO::uGetOne(User::class, "email=? and password= ?",false,[$email,$password]);
			$user->setOnline(true);
			DAO::update($user);
			return $user;
			//TODO
			//Loading from the database the user corresponding to the parameters
			//Checking user creditentials
			//Returning the user
		}
		return;
	}
	
	
	

    /**
     * {@inheritDoc}
     * @see \Ubiquity\controllers\auth\AuthController::terminate()
     */
    public function terminate()
    {
        $user=USession::get($this->_getUserSessionKey());
        $user->setOnline(false);
        DAO::update($user);
        USession::terminate();
        $fMessage=new FlashMessage("You have been properly disconnected!","Logout","success","checkmark");
        $this->terminateMessage($fMessage);
        $message=$this->fMessage($fMessage);
        $this->authLoadView($this->_getFiles()->getViewNoAccess(),["_message"=>$message,"authURL"=>$this->getBaseUrl(),"bodySelector"=>$this->_getBodySelector(),"_loginCaption"=>$this->_loginCaption]);
        
    }

    /**
	 * {@inheritDoc}
	 * @see \Ubiquity\controllers\auth\AuthController::isValidUser()
	 */
	public function _isValidUser($action=null) {
		return USession::exists($this->_getUserSessionKey());
	}

	public function _getBaseRoute() {
		return 'BaseAuthController';
	}
	
	/**
	* {@inheritDoc}
	* @see \Ubiquity\controllers\auth\AuthController::_getLoginInputName()
	*/
	public function _getLoginInputName() {
	    return "email";
	}
	/**
	 * {@inheritDoc}
	 * @see \Ubiquity\controllers\auth\AuthController::_getPasswordInputName()
	 */
	public function _getPasswordInputName() {
	    return "password";
	}
	

}
