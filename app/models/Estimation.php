<?php
namespace models;
class Estimation{
	/**
	 * @id
	 * @column("name"=>"id","nullable"=>false,"dbType"=>"int(11)")
	*/
	private $id;

	/**
	 * @column("name"=>"points","nullable"=>true,"dbType"=>"varchar(10)")
	*/
	private $points;

	/**
	 * @manyToOne
	 * @joinColumn("className"=>"models\\Story","name"=>"idStory","nullable"=>false)
	*/
	private $story;

	/**
	 * @manyToOne
	 * @joinColumn("className"=>"models\\User","name"=>"idUser","nullable"=>false)
	*/
	private $user;

	 public function getId(){
		return $this->id;
	}

	 public function setId($id){
		$this->id=$id;
	}

	 public function getPoints(){
		return $this->points;
	}

	 public function setPoints($points){
		$this->points=$points;
	}

	 public function getStory(){
		return $this->story;
	}

	 public function setStory($story){
		$this->story=$story;
	}

	 public function getUser(){
		return $this->user;
	}

	 public function setUser($user){
		$this->user=$user;
	}

	 public function __toString(){
	     $ret="";
	     if (isset($this->user)) {
	         $ret=$this->user." - ";
	     }
		return $ret.$this->points;
	}

}