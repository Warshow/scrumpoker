<?php
namespace models;
class User{
	/**
	 * @id
	 * @column("name"=>"id","nullable"=>false,"dbType"=>"int(11)")
	*/
	private $id;

	/**
	 * @column("name"=>"name","nullable"=>true,"dbType"=>"varchar(45)")
	*/
	private $name;

	/**
	 * @column("name"=>"email","nullable"=>true,"dbType"=>"varchar(255)")
	*/
	private $email;

	/**
	 * @column("name"=>"password","nullable"=>true,"dbType"=>"varchar(45)")
	*/
	private $password;
	
	/**
	 * @column("name"=>"online","nullable"=>true,"dbType"=>"boolean")
	 */
	private $online;

	/**
	 * @oneToMany("mappedBy"=>"user","className"=>"models\\Estimation")
	*/
	private $estimations;

	/**
	 * @oneToMany("mappedBy"=>"user","className"=>"models\\Project")
	*/
	private $projects;
	
	/**
	 * @manyToMany("targetEntity"=>"models\\Project","inversedBy"=>"users")
	 * @joinTable("name"=>"project_has_user")
	 */
	private $participations;
	

	 /**
     * @return mixed
     */
    public function getOnline()
    {
        return $this->online;
    }

    /**
     * @param mixed $online
     */
    public function setOnline($online)
    {
        $this->online = $online;
    }

    /**
     * @return mixed
     */
    public function getParticipations()
    {
        return $this->participations;
    }

    /**
     * @param mixed $participations
     */
    public function setParticipations($participations)
    {
        $this->participations = $participations;
    }

    public function getId(){
		return $this->id;
	}

	 public function setId($id){
		$this->id=$id;
	}

	 public function getName(){
		return $this->name;
	}

	 public function setName($name){
		$this->name=$name;
	}

	 public function getEmail(){
		return $this->email;
	}

	 public function setEmail($email){
		$this->email=$email;
	}

	 public function getPassword(){
		return $this->password;
	}

	 public function setPassword($password){
		$this->password=$password;
	}

	 public function getEstimations(){
		return $this->estimations;
	}

	 public function setEstimations($estimations){
		$this->estimations=$estimations;
	}

	 public function getProjects(){
		return $this->projects;
	}

	 public function setProjects($projects){
		$this->projects=$projects;
	}
	

	 public function __toString(){
		return $this->name;
	}

}